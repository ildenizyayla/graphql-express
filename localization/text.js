// eslint-disable-next-line import/prefer-default-export
export const textResource = {
    en: {
        user: {
            alreadyExists: 'User exists already',
            credentialsError: 'Please check credentials',
        },
        err: {
            unauthenticated: 'Unauthenticated',
        },
    },
};
