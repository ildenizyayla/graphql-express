import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

import * as locals from '../../helpers/local';
import User from '../../models/user';
import { secretKey } from '../../settings';

const passwordSalt = 12;
const expiration = 1;

export default {
    createUser: async (args) => {
        try {
            const existingUser = await User.findOne({ email: args.userInput.email });
            if (existingUser) {
                throw new Error(locals.getText('user', 'alreadyExists'));
            }
            const hashedPassword = await bcrypt.hash(args.userInput.password, passwordSalt);

            const user = new User({
                email: args.userInput.email,
                password: hashedPassword,
            });

            const result = await user.save();

            return { ...result._doc, password: null, _id: result.id };
        } catch (err) {
            throw err;
        }
    },
    login: async ({ email, password }) => {
        const user = await User.findOne({ email });

        if (!user || !(await bcrypt.compare(password, user.password))) {
            throw new Error(locals.getText('user', 'credentialsError'));
        }

        const token = jwt.sign(
            { userId: user.id, email: user.email },
            secretKey,
            {
                expiresIn: `${expiration}h`,
            },
        );

        return { userId: user.id, token, tokenExpiration: expiration };
    },
};
