/* eslint-disable no-use-before-define */
import Event from '../../models/event';
import User from '../../models/user';
import { dateToString } from '../../helpers/date';

const events = async (eventIds) => {
    try {
        const res = await Event.find({ _id: { $in: eventIds } });
        return res.map(event => transformEvent(event));
    } catch (err) {
        throw err;
    }
};

const singleEvent = async (eventId) => {
    try {
        const event = await Event.findById(eventId);

        return transformEvent(event);
    } catch (err) {
        throw err;
    }
};

const user = async (userId) => {
    try {
        const result = await User.findById(userId);

        return {
            ...result._doc,
            _id: result.id,
            createdEvents: events.bind(this, result._doc.createdEvents),
            password: null,
        };
    } catch (err) {
        throw err;
    }
};

export const transformEvent = event => ({
    ...event._doc,
    _id: event.id,
    date: dateToString(event._doc.date),
    creator: user.bind(this, event.creator),
});

export const transformBooking = booking => ({
    ...booking._doc,
    _id: booking.id,
    user: user.bind(this, booking._doc.user),
    event: singleEvent.bind(this, booking._doc.event),
    createdAt: dateToString(booking._doc.createdAt),
    updatedAt: dateToString(booking._doc.updatedAt),
});
