import Event from '../../models/event';
import User from '../../models/user';
import { getText } from '../../helpers/local';
import { transformEvent } from './merge';

export default {
    events: async () => {
        try {
            const events = await Event.find();
            return events.map(event => transformEvent(event));
        } catch (err) {
            throw err;
        }
    },
    createEvent: async (args, req) => {
        if (!req.isAuth) {
            throw new Error(getText('err', 'unauthenticated', '!'));
        }
        const event = new Event({
            title: args.eventInput.title,
            description: args.eventInput.description,
            price: +args.eventInput.price,
            date: new Date(args.eventInput.date),
            creator: req.userId,
        });
        let createdEvent;
        try {
            const result = await event.save();
            createdEvent = transformEvent(result);
            const creator = await User.findById(req.userId);

            if (!creator) {
                throw new Error('User not found.');
            }
            creator.createdEvents.push(event);
            await creator.save();

            return createdEvent;
        } catch (err) {
            throw err;
        }
    },
};
