import authResolver from './auth';
import eventsResolver from './events';
import bookingResolver from './booking';

const rootResolver = {
    ...authResolver,
    ...eventsResolver,
    ...bookingResolver,
};

export default rootResolver;
