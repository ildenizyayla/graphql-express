export const booking = `type Booking {
    _id: ID!
    event: Event!
    user: User!
    createdAt: String!
    updatedAt: String!
}`;

export const event = `type Event {
    _id: ID!
    title: String!
    description: String!
    price: Float!
    date: String!
    creator: User!
}`;

export const user = `type User {
    _id: ID!
    email: String!
    password: String
    createdEvents: [Event!]
}`;

export const authData = `type AuthData {
    userId: ID!
    token: String!
    tokenExpiration: Int!
}`;

export const eventInput = `input EventInput {
  title: String!
  description: String!
  price: Float!
  date: String!
}`;

export const userInput = `input UserInput {
  email: String!
  password: String!
}`;

export const rootQuery = `type RootQuery {
    events: [Event!]!
    bookings: [Booking!]!
    login(email: String!, password: String!): AuthData!
}`;

export const rootMutation = `type RootMutation {
    createEvent(eventInput: EventInput): Event
    createUser(userInput: UserInput): User
    bookEvent(eventId: ID!): Booking!
    cancelBooking(bookingId: ID!): Event!
}`;

export const schema = `schema {
    query: RootQuery
    mutation: RootMutation
}`;
