import { buildSchema } from 'graphql';
import * as items from './items';

export default buildSchema(`
 ${items.booking}
 ${items.event}
 ${items.user}

 ${items.authData}
 ${items.eventInput}
 ${items.userInput}

 ${items.rootQuery}
 ${items.rootMutation}

 ${items.schema}
`);
