import express from 'express';
import bodyParser from 'body-parser';
import graphqlHttp from 'express-graphql';
import mongoose from 'mongoose';
import compression from 'compression';
import morgan from 'morgan';

import graphQlSchema from './graphql/schema/index';
import graphQlResolvers from './graphql/resolvers/index';
import isAuth from './middleware/isAuth';
import header from './middleware/header';
import errorHandler from './middleware/error';
import winston from './logs/winston';

import { apiPort, graphQlUri } from './settings';

const app = express();

// configure the app
app.use(bodyParser.json());
app.use(compression());
app.use(morgan('combined', { stream: winston.stream }));
app.use(header);
app.use(isAuth);
app.use(errorHandler);
app.use(
    `/${graphQlUri}`,
    graphqlHttp({
        schema: graphQlSchema,
        rootValue: graphQlResolvers,
        graphiql: true,
    }),
);

// redirect all nonmatching routes
app.all('*', (req, res) => res.statusCode(301).redirect('https://google.com/'));

app.disable('x-powered-by');

// connect to mongo
mongoose
    .connect(
        `mongodb+srv://${process.env.MONGO_USER}:${
            process.env.MONGO_PASSWORD
        }@${process.env.MONGO_CLUSTER}/${process.env.MONGO_DB}?retryWrites=true`,
        { useNewUrlParser: true },
    )
    .then(() => {
        app.listen(apiPort);
    })
    .catch((err) => {
        // eslint-disable-next-line no-console
        console.log(err);
    });

// eslint-disable-next-line no-console
mongoose.connection.on('connected', () => console.log(`Mongoose default connection open to ${process.env.MONGO_DB}`));
