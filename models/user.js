import { Schema, model } from 'mongoose';

export default model(
    'User',
    new Schema({
        email: {
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: true,
        },
        createdEvents: [
            {
                type: Schema.Types.ObjectId,
                ref: 'Event',
            },
        ],
    }),
);
