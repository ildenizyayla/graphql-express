import { Schema, model } from 'mongoose';

export default model(
    'Booking',
    new Schema(
        {
            event: {
                type: Schema.Types.ObjectId,
                ref: 'Event',
            },
            user: {
                type: Schema.Types.ObjectId,
                ref: 'User',
            },
        },
        { timestamps: true },
    ),
);
