import { Schema, model } from 'mongoose';

export default model(
    'Event',
    new Schema({
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        price: {
            type: Number,
            required: true,
        },
        date: {
            type: Date,
            required: true,
        },
        creator: {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
    }),
);
