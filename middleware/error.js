/* eslint-disable no-unused-vars */
import winston from '../logs/winston';

export default (err, req, res, next) => {
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

    res.status(500).send('{error: "error occured"}');
};
