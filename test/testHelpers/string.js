/* eslint-disable import/prefer-default-export */
const ans = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const puncs = ',.<>?!@#$%^&*()_*+-="/;|{}';

export const randomString = (len = 1, isAlpaNum = true) => {
    const str = isAlpaNum ? ans : puncs;
    let res = '';

    for (let i = 0; i < len; i++) {
        const p = Math.floor(Math.random() * str.length);
        res += str[p - 1];
    }
    return res;
};
