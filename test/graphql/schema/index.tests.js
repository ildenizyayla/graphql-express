import mocha from 'mocha';
import chai from 'chai';

import schema from '../../../graphql/schema/index';

const propertyCheck = (fieldDef, fields) =>
    mocha.it(`${fieldDef[0]} field exists with property type`, () => {
        chai.expect(fields).to.have.property(fieldDef[0]);
        chai.expect(fields[fieldDef[0]].type.toString()).equals(fieldDef[1]);
    });

mocha.describe('Schema Tests', () => {
    const cases = [
        {
            case: 'Booking',
            fieldType: 'Type',
            typeDefs: [
                ['_id', 'ID!'],
                ['event', 'Event!'],
                ['user', 'User!'],
                ['createdAt', 'String!'],
                ['updatedAt', 'String!'],
            ],
        },
        {
            case: 'Event',
            fieldType: 'Type',
            typeDefs: [
                ['_id', 'ID!'],
                ['title', 'String!'],
                ['description', 'String!'],
                ['price', 'Float!'],
                ['date', 'String!'],
                ['creator', 'User!'],
            ],
        },
    ];

    // eslint-disable-next-line array-callback-return
    cases.map((x) => {
        const fields = schema.getType(x.case).getFields();
        mocha.describe(`${x.case} Type Tests`, () => {
            x.typeDefs.map(y => propertyCheck(y, fields));
        });
    });
});
