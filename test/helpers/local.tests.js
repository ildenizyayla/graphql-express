import mocha from 'mocha';
import chai from 'chai';

import * as sbjct from '../../helpers/local';
import * as localization from '../../localization/text';
import * as stringHelper from '../testHelpers/string';

mocha.describe('date Tests', () => {
    mocha.describe('dateToString Tests', () => {
        const errorMessage = 'Cannot read property \'null\' of undefined';
        const lang = 'en';
        const obj = 'user';
        const rKey = 'alreadyExists';
        const punc = stringHelper.randomString(1, false);

        mocha.it('When VALID object WITH punctuation is requested, should return the resource with punctuation', () =>
            chai.expect(sbjct.getText(obj, rKey, punc, lang)).equal(`${localization.textResource[lang][obj][rKey]}${punc}`));

        mocha.it('When VALID object WITHOUT punctuation is requested, should return the resource without punctuation', () =>
            chai.expect(sbjct.getText(obj, rKey)).equal(`${localization.textResource[lang][obj][rKey]}`));

        mocha.it('When INVALID object props are passed, should throw', () =>
            chai.expect(() => sbjct.getText(null, null)).to.throw(errorMessage));
    });
});
