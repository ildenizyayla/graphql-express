import mocha from 'mocha';
import chai from 'chai';

import * as sbjct from '../../helpers/date';

mocha.describe('date Tests', () => {
    const dt = new Date();
    const errorMessage = 'Invalid time value';

    mocha.describe('dateToString Tests', () => {
        const result = sbjct.dateToString(dt);

        mocha.it('When date is VALID, should return an iso date string', () =>
            chai.expect(result.length).greaterThan(0) && chai.expect(result).equal(dt.toISOString()));

        mocha.it('When date is INVALID, should throw', () =>
            chai.expect(() => sbjct.dateToString('')).to.throw(errorMessage));
    });
});
