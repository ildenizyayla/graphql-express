# GraphQL API
The code originally belongs to Maximilian Schwarzmuller's tutorial series [link](https://github.com/academind/yt-graphql-react-event-booking-api.git).

It has been refactored and upgraded to ES6, the server capabilities have been enriched and tests were added in order to develop a 'GraphQL API with Node-Express' boilerplate.

## Getting Started

### Prerequisites
Node environment must be available for run time and a Mongo DB instance is needed for storage.

### Installation
The application does not require any pre-installation, other than restoring NPM packages which can be achieved by regular npm install command.

**1**. Modify **setting.js** and **nodemon.json** files.


**2**. Install dependencies.
```sh
npm install
```


**3**. Run the server.
```sh
npm start
```


## Tests
Using Mocha runner and Chai assertion, some test were added to the project in order to establish a baseline. 
There is also a Postman collection under documentation folder which can be used to check common errors either by using Postman and Graph QL UI's interface.


To run tests:
```sh
npm test
```

To run test with coverage output:
```sh
npm run test-with-coverage
```

## Deployment
Due to the purpose of this repository, a deployment has not been foreseen which would depend upon to the project's requirements, especially to the physical or cloud server. 


## License
This project is not licensed. Please feel free to use it but it would be nice to cite Mr. Schwarzmuller's work.
