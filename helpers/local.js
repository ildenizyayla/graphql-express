import { textResource } from '../localization/text';
import { defaults } from '../settings';

// eslint-disable-next-line import/prefer-default-export
export const getText = (objName, resourceKey, punctuation = '', lang = defaults.lang) => `${textResource[lang][objName][resourceKey]}${punctuation}`;
