// eslint-disable-next-line import/prefer-default-export
export const dateToString = date => new Date(date).toISOString();
