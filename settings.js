export const apiPort = 8000;
export const graphQlUri = 'graphql';
export const secretKey = 'API_SECRET_KEY';
export const defaults = {
    lang: 'en',
};
